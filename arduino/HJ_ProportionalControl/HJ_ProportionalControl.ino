// ProportionalControl.pde
// -*- mode: C++ -*-
//
// Make a single stepper follow the analog value read from a pot or whatever
// The stepper will move at a constant speed to each newly set posiiton, 
// depending on the value of the pot.
//
// Copyright (C) 2012 Mike McCauley
// $Id: ProportionalControl.pde,v 1.1 2011/01/05 01:51:01 mikem Exp mikem $

#include <AccelStepper.h>

// Define a stepper and the pins it will use

AccelStepper stepper(AccelStepper::DRIVER, 9, 10);
// This defines the analog input pin for reading the control voltage
// Tested with a 10k linear pot between 5v and G  ND
#define ANALOG_IN A1


#include <SerialCommand.h>

SerialCommand serialCommand;

int angle = 0;

void setup()
{  
  stepper.setMaxSpeed(3000);
  Serial.begin(9600);
  while (!Serial);
  serialCommand.addCommand("getData", dataSerializer);
}

void loop()
{
  // Read new position
  int analog_in = analogRead(ANALOG_IN);
  if (abs(angle - analog_in) > 2) {
    angle = analog_in;
  }
   
  //Serial.println(analog_in);
  stepper.moveTo(-angle);
  stepper.setSpeed(3000);
  stepper.runSpeedToPosition();
  if ( Serial.available() > 0) {
      serialCommand.readSerial();
      
  }
}

void dataSerializer() {
  Serial.println(angle);
}
