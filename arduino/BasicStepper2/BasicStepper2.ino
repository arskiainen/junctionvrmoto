#include <Arduino.h>
#include "BasicStepperDriver.h"

#define MOTOR_STEPS 200
#define MICROSTEPS 16
#define MICROSTEPS32 32


#define RIGHT_DIR 12
#define RIGHT_STEP 11

#define LEAN_DIR 10
#define LEAN_STEP 9

#define LEFT_DIR 8
#define LEFT_STEP 7
  

BasicStepperDriver right(MOTOR_STEPS, RIGHT_DIR, RIGHT_STEP);
BasicStepperDriver left(MOTOR_STEPS, LEFT_DIR, LEFT_STEP);
BasicStepperDriver lean(MOTOR_STEPS, LEAN_DIR, LEAN_STEP);

double m = 200;
double h = 0.5;
double base = 1.5;
double v = 20;

//double alpha = 0;
double theta = 0;



double calcAngularAcceleration(double alpha){
  double M = calcCentripetalForce(m, v, calcTurnRadius(alpha)) * h;
  Serial.print(M);
  Serial.print(" "); 
  return M/calcInertialMomentum(); 
}

double calcTurnRadius(double alpha){
  double R1 = base / sin(alpha);
  double R2 = base / tan(alpha);
  double R = (R1+R2)/2;
  double r = R*cos(theta); 
  Serial.print(r);
  Serial.print(" ");
  return r;
}

double calcCentripetalForce(double m, double v, double r){
  return m*v*v/r; 
}

double calcInertialMomentum(){
  return 0.33*m; // *L = *1;
}

double getSteeringAngle(){
  return max(0, (analogRead(A1)-520)/10.0);
}


void setup() {
    right.setRPM(20);
    left.setRPM(20);
    lean.setRPM(20);
    
    right.setMicrostep(MICROSTEPS);
    left.setMicrostep(MICROSTEPS);
    lean.setMicrostep(MICROSTEPS);

    Serial.begin(9600);
}

void loop() {
  //right.move(20*MICROSTEPS);
  //right.move(-20*MICROSTEPS);

  //left.move(20*MICROSTEPS);
  //left.move(-20*MICROSTEPS);

  //lean.move(40*MICROSTEPS);
  //lean.move(-40*MICROSTEPS);
  
  //int throttle = analogRead(A0);
  //Serial.println(throttle);

  
                                                                                                                                           
  Serial.print(" ");
  double acc = calcAngularAcceleration(steering_angle/57);
  
  Serial.println(acc);
  delay(100);
}
