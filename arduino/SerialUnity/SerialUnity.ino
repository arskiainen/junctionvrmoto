#include <SerialCommand.h>

SerialCommand serialCommand;

void setup() {
  Serial.begin(9600);
  while (!Serial);

  serialCommand.addCommand("getData", dataSerializer);
  }
  
  
 void loop() {
 
  if ( Serial.available() > 0) {
      serialCommand.readSerial();
  delay(50);  
  }
}

void dataSerializer ()
{
  int velocity = 36;
  int steerAngle = 4;
  int throttle = 437;
  int radius = 25;
  int leanAcc = 673;
  String serialized = "{\"velocity\":";
  serialized += velocity;
  serialized += ",\"steerAngle\":";
  serialized += steerAngle;
  serialized += ",\"throttle\":";
  serialized += throttle;
    serialized += ",\"radius\":";
  serialized += radius;
    serialized += ",\"leanAcc\":";
  serialized += leanAcc;
  serialized += "}";
  Serial.println(serialized);
}
