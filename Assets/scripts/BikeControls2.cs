﻿using UnityEngine;
using System.Collections;

public class BikeControls2 : MonoBehaviour {

	public float fullTorque;
	public float tiltFac;
	public float maxRPM;
	public float maxAccel;
	public float maxBraking;
	public float maxSpeed;

	public Transform cameraRig;


	private Quaternion originalRotation;
	private float currentVelocity;
	private float currentRPM;
	private float currentBraking;
	private float currentAccel;
	private float currentTilt;

	private Vector3 frontWheelPos;
	private Vector3 rearWheelPos;

	private Wheel frontWheel;
	private Wheel rearWheel;

	private int clutch = 0;

	private Rigidbody bike;

	private bool fwGrounded;
	private bool rwGrounded;


	// Use this for initialization
	void Start () {
		bike = transform.GetComponent<Rigidbody> ();
		bike.centerOfMass = transform.FindChild ("CenterOfMass").localPosition;
		originalRotation = bike.transform.localRotation;
		frontWheel = transform.FindChild ("Frontwheel").GetComponent<Wheel> ();
		rearWheel = transform.FindChild ("Rearwheel").GetComponent<Wheel> ();
		frontWheelPos = frontWheel.transform.localPosition;
		rearWheelPos = rearWheel.transform.localPosition;
	}

	// Update is called once per frame
	void Update () {

		cameraRig.position = transform.position;
		cameraRig.localRotation = Quaternion.Euler(new Vector3(cameraRig.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, cameraRig.localRotation.eulerAngles.z));
		Quaternion localRotation = transform.localRotation;
		float horiz = Input.GetAxis ("Horizontal");
		float vert = Input.GetAxis ("Vertical");
		Quaternion tilt = MagicSteering (horiz, localRotation);
		if (vert > 0) {
			currentBraking = 0;
			currentRPM = Mathf.Clamp (currentRPM + maxAccel * vert, 0, maxRPM);
			clutch = 1;
		} else if (vert < 0) {
			currentRPM = Mathf.Clamp (currentRPM - maxAccel, 0, maxRPM);
			currentBraking = Mathf.Clamp (currentBraking + maxBraking * vert, 0, maxBraking);
			clutch = 0;
		} else {
			currentBraking = 0;
			currentRPM = Mathf.Clamp (currentRPM - maxAccel, 0, maxRPM);
			clutch = 0;
		}
	}

	void FixedUpdate () {
		if (rearWheel.isGrounded()) {
			bike.AddForceAtPosition (bike.transform.forward * clutch * fullTorque * currentRPM, rearWheel.GetForcePoint());
			bike.AddForceAtPosition (bike.transform.forward * currentBraking * 0.3f, rearWheel.GetForcePoint());
		}
		if (frontWheel.isGrounded ()) {
			bike.AddForceAtPosition (bike.transform.forward * currentBraking, frontWheel.GetForcePoint());
		}

	}

	Quaternion MagicSteering (float horiz, Quaternion localRotation) {
		//Debug.Log (localRotation);

		return localRotation;
	}
}
