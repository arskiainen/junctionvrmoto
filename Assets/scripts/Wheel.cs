﻿using UnityEngine;
using System.Collections;

public class Wheel : MonoBehaviour {

	private Vector3 forcePoint;

	private Transform debugSphere;

	private float wheelRadius = 0.31f;
	private float contactRadius = 0.05f;

	void Start() {
		//debugSphere = transform.FindChild ("DebugSphere");
		//debugSphere.SetParent (null);
	}

	public Vector3 GetForcePoint () {
		return forcePoint;
	}

	public bool isGrounded() { 
		RaycastHit hitInfo;

		//Vector3 relativeDown = new Vector3( -transform.up.x, -transform.up.y, -Vector3.up.z) * (wheelRadius-contactRadius);
		//Debug.Log (relativeDown);
		//debugSphere.position = transform.position + relativeDown;


		float angleDelta = Vector3.Angle(Vector3.right, transform.up - Vector3.up);
		if (angleDelta > 90) {
			angleDelta = 180 - angleDelta;
		}
		float tweakedAngle = angleDelta/90;
		Debug.Log (tweakedAngle);
		float distanceMax = (tweakedAngle) * (wheelRadius)/2 + (wheelRadius)/2;
		Debug.Log (distanceMax);
		Physics.Raycast (transform.position, -Vector3.up, out hitInfo, distanceMax);
		if (hitInfo.collider != null) {
			if (hitInfo.collider.transform.CompareTag ("Ground")) {
				return true;
				forcePoint = hitInfo.point;
			}
		}
		return false;
	}

}
