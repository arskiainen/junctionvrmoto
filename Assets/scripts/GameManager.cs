﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ArdData
{
	public float velocity;
	public float steerAngle; // 0-1000
	public float throttle; // 0-1000
	public float radius;
	public float leanAcc;
}

public class GameManager : MonoBehaviour {

	public BikeControls3 bike;

	private ArduinoController ardCon;

	ArdData ardData = new ArdData();

	void Start() {
		ardCon = transform.GetComponent<ArduinoController> ();
		ardCon.Open ();
	}
	// Update is called once per frame
	void Update () {
		
		ardCon.WriteToArduino ("getData");
		string message = ardCon.ReadFromArduino ();
		if (message != null) {
			Debug.Log (message);
			bike.SetAngle (int.Parse(message));
			//ardData = JsonUtility.FromJson<ArdData> (message);
			//Debug.Log (ardData.velocity);
			//Debug.Log (ardData.steerAngle);
			//Debug.Log (ardData.throttle);
			//Debug.Log (ardData.radius);
			//Debug.Log (ardData.leanAcc);
		}

		if(Input.GetKeyDown(KeyCode.Escape)) {
			ardCon.Close ();
		}
	}
}
