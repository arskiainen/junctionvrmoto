﻿using UnityEngine;
using System.Collections;

public class BikeControls : MonoBehaviour {

	public float fullTorque;
	public float tiltFac;

	private Quaternion tilt;
	private Quaternion localRotation;

	private float horiz;
	private float vert;
	public WheelCollider[] m_WheelColliders = new WheelCollider[2];

	// Use this for initialization
	void Start () {
		transform.GetComponentInChildren<WheelCollider>().ConfigureVehicleSubsteps(1, 9, 15);
	}
	
	// Update is called once per frame
	void Update () {
		localRotation = transform.localRotation;
		horiz = Input.GetAxis ("Horizontal");
		vert = Input.GetAxis ("Vertical");
		tilt = MagicSteering (horiz, localRotation);
		if (vert > 0) {
			m_WheelColliders [0].brakeTorque = 0;
			m_WheelColliders [1].brakeTorque = 0;
			m_WheelColliders [0].motorTorque = fullTorque * vert;
		} else if (vert < 0) {
			m_WheelColliders [0].motorTorque = 0;
			m_WheelColliders [0].brakeTorque = 10000 * -vert;
			m_WheelColliders [1].brakeTorque = 20000 * -vert;
		} else {
			m_WheelColliders [0].motorTorque = 0;
			m_WheelColliders [0].brakeTorque = 0;
			m_WheelColliders [1].brakeTorque = 0;
		}
		//m_WheelColliders [0].motorTorque = m_WheelColliders [0].motorTorque * 0.8f;
	}

	 

	Quaternion MagicSteering (float horiz, Quaternion localRotation) {
		Debug.Log (localRotation);

		return localRotation;
	}
}
