﻿using UnityEngine;
using System;
using System.Collections;
using System.IO.Ports;

public class ArduinoController : MonoBehaviour {

	public string port = "COM3";
	public int baudrate = 9600;
	private SerialPort stream;



	public void Open() {
		stream = new SerialPort (port, baudrate);
		stream.ReadTimeout = 50;
		stream.Open ();
	}

	public void Close() {
		stream.Close ();
	}

	public void WriteToArduino(string message) {
		stream.WriteLine (message);
		stream.BaseStream.Flush ();
	}

	public string ReadFromArduino(int timeout = 50) {
		stream.ReadTimeout = timeout;
		try {
			return stream.ReadLine();
		}
		catch (TimeoutException) {
			return null;
		}
	}
}
