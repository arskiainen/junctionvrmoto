﻿using UnityEngine;
using System.Collections;

public class BikeControls3 : MonoBehaviour {


	public Transform cameraRig;

	private Quaternion originalRotation;
	private float currentVelocity;
	private float currentLeanAcc;
	private float currentTilt;
	private float currentSteerAngle; 
	private float currentRadius;

	public float maxVelocity = 20f;

	private Rigidbody bike;

	public void SetAngle(float angle) {
		currentTilt = (angle - 510) / 120; // 450 - 570
	}
	public void SetVelocity(float velocity) {
		//currentVelocity = (velocity - 500) / 200;
		currentVelocity = velocity;
	}

	// Use this for initialization
	void Start () {
		bike = transform.GetComponent<Rigidbody> ();
		originalRotation = bike.transform.localRotation;
		//currentVelocity = 70f;
	}

	// Update is called once per frame
	void Update () {

		cameraRig.position = transform.position;
		cameraRig.localRotation = Quaternion.Euler(new Vector3(cameraRig.localRotation.eulerAngles.x, transform.localRotation.eulerAngles.y, cameraRig.localRotation.eulerAngles.z));

		Quaternion localRotation = transform.localRotation;

		//temporary
		float horiz = Input.GetAxis ("Horizontal");
		float vert = Input.GetAxis ("Vertical");
		if (horiz != 0) {
			SetAngle ((-horiz * (1 - currentVelocity * 0.2f / maxVelocity) + 1) * 60 + 450);
		}
		Debug.Log (currentTilt);
		Debug.Log (bike.rotation.eulerAngles);


		if (vert > 0) {
			//accelerating
			SetVelocity (Mathf.Clamp (currentVelocity + vert, 0, maxVelocity));

		} else if (vert < 0) {
			SetVelocity (Mathf.Clamp (currentVelocity - vert, 0, maxVelocity));
		} else {
			SetVelocity (Mathf.Clamp (currentVelocity - 0.5f, 0, maxVelocity));
		}

		Debug.Log (currentVelocity);
		Vector3 localVelocity = transform.InverseTransformVector(bike.velocity);
		localVelocity.z = currentVelocity;
		localVelocity.x = 0;
		localVelocity.y = 0;
		bike.velocity = transform.TransformVector (localVelocity);
	}

	void FixedUpdate () {
		float turning = 0;
		if (currentVelocity > 0) {
			 turning = currentTilt * 100f * Time.fixedDeltaTime;
		}	
			float newYRotation = bike.rotation.eulerAngles.y + turning;
		bike.MovePosition (bike.position + bike.velocity * Time.fixedDeltaTime);
		Debug.Log (bike.velocity);
		bike.MoveRotation( Quaternion.Euler(new Vector3
			(
				bike.rotation.eulerAngles.x,
				newYRotation,
				-currentTilt*60
			)));
	}
		
}
